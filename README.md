For kids who like to read or to be read to your kids

The list:
- hardy boys
- nancy drew
- dory phantasmagory
- roald dahl
- phantom tollbooth
- hobbit, LOTR
- Harry Potter
- encyclopaedia brown
- the XX story treehouse
- diary of a wimpy kid
- dog man
- captain underpants
- indian in the cupboard
- chronicles of narnia
- percy jackson
- magic treehouse

Finding books you can't quite remember:
- https://www.nypl.org/blog/2017/11/22/finding-book-forgotten-title?page=1
